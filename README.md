# README #
### Introduction ###
Exercise Day 19-20: Development workflow: Git.

### Git workflow ###
- Create your own git project with 3 files below
    - file_a.js
    - file_b.js
    - file_c.js
- Create a new branch: staging based on the master branch
- Create a new branch: branch_a based on the master branch
    - Change the file_a.js and commit the change to branch_a
- Create a new branch: branch_b basedon the master branch
    - Change the file_b.js and commit the change to branch_b
- Create a new branch: branch_c based on the master branch
    - Change the file_c.js
    - Change the file_a.js (change the same row as the branch_a)
    - then commit the change to branch_c
- Merge branch_a to staging
- Merge branch_b to staging
- Merge branch_c to staging & fix the conflict
- Merge the branch_a to master
- Merge the branch_b to master
- Merge the branch_c to master